import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { UsersComponent } from './users/users.component';
import { ServicesComponent } from './services/services.component';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule} from '@angular/forms';
import { ToggleViewMoreDirective } from './toggle-view-more.directive';
import {ServiceForChartService} from "./services/service-for-chart.service";
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    NavBarComponent,
    UsersComponent,
    ServicesComponent,
    ToggleViewMoreDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserModule,
    HttpClientModule
  ],
  providers: [ServiceForChartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
