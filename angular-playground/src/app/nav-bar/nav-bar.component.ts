import {Component, Input, OnChanges, OnInit} from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit, OnChanges {
  @Input() isOpen: boolean;
  open = true;
  ngOnChanges() {
      setTimeout(()=>{
        this.open = !this.open;
      }, 100);
  }
  constructor() { }

  ngOnInit() {
  }



}
