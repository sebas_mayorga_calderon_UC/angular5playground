import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  navbarOpen: boolean;
  navBarOpenArray: boolean[] = [];
  ngOnInit(): void {
    this.navbarOpen = false;
    this.navBarOpenArray[0] = false;
  }
  toggleNavBar(){
    this.navbarOpen = !this.navbarOpen;
  }
}
