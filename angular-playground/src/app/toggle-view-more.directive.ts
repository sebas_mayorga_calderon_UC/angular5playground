import {Directive, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appToggleViewMore]'
})
export class ToggleViewMoreDirective {
  @Input() mouseOver: boolean = false;
  @Input() mouse = [];
  @Input() index: number;
  @Input() function;
  constructor() {
    this.mouse[this.index]= false;
  }


}
