import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from "@angular/common/http";
import 'rxjs/add/operator/map';

@Injectable()
export class ServiceForChartService {
  testInfo = [
    {name: 'Sebas', age: 19},
    {name: 'Mario', age: 20},
    {name: 'Maria', age: 21},
    {name: 'Menaline', age: 20}
  ];
  constructor( private _http: HttpClient) { }
  getTestInfo() {
   return this.testInfo.slice();
  }

}
