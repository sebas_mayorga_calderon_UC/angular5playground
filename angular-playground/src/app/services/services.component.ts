import { Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ServiceForChartService} from './service-for-chart.service';
import { Chart } from 'chart.js';
@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  @ViewChild('chartJSPie') chartJSPie: ElementRef;
  @ViewChild('chartPie') chartPie: ElementRef;
  values = [
    {
      gasto: 'netflix',
      monto: 15000
    },
    {
      gasto: 'Hulu',
      monto: 25000
    },
    {
      gasto: 'Casa',
      monto: 200000
    },
    {
      gasto: 'agua',
      monto: 40000
    },
    {
      gasto: 'luz',
      monto: 30000
    }
  ];
  chartJS = [];
  constructor(private chartService: ServiceForChartService) { }
  ngOnInit() {
    this.basicPieChart();
    this.createChartJSChart();
  }
  basicPieChart() {
    const element =  this.chartPie.nativeElement;
    const dataPie = [{
      values: this.values.map(item => item.monto ),
      labels: this.values.map( item => item.gasto),
      type: 'pie'
    }];
    const layout = {
      width: '400'
    };
    Plotly.newPlot(element, dataPie, layout);
  }
  createChartJSChart() {
    const config = {
      type: 'pie',
      data: {
        datasets: [{
          data: this.values.map( item => item.monto ),
          backgroundColor: [
            '#4286f4',
            '#48af23',
            '#fcea4b',
            '#e82b1e',
            '#ce21a8',
          ],
          label: 'Dataset 1'
        }],
        labels: this.values.map( item => item.gasto )
      },
      options: {
        responsive: true
      }
    };
    const el = this.chartJSPie.nativeElement.getContext('2d');
    this.chartJS = new Chart(el, config);
  }
  randomScalingFactor() {
    return Math.round(Math.random() * 100);
  };


}
