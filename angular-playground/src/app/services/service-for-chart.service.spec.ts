import { TestBed, inject } from '@angular/core/testing';

import { ServiceForChartService } from './service-for-chart.service';

describe('ServiceForChartService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServiceForChartService]
    });
  });

  it('should be created', inject([ServiceForChartService], (service: ServiceForChartService) => {
    expect(service).toBeTruthy();
  }));
});
